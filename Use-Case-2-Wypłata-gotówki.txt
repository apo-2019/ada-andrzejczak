Use Case 2: Wypłata gotówki
=====================

**Aktor podstawowy:** Klient


Główni odbiorcy i oczekiwania względem systemu:
-----------------------------------------------

- Klient: oczekuje możliwości szybkiego wprowadzenia danych,   

- Klient: oczekuje możliwości wypłaty, 

- Bank:  oczekuje popranie przeprowadzonie operacji,

- Bank:  oczekuje stałej łączności z bankomatem. 

Warunki wstępne:
----------------

-Klient jest zidentyfikowany w procesie autoryzacji kilenta (use case 1).

- Bankomat jest sprawny i uzupełniony gotówką.



Warunki końcowe:
----------------
 Wypłata gotówki przebiegła poprawnie.  
 Opreacja jest bezpieczna.
 Operacja została zaksięgowana przez bank.


Scenariusz główny (ścieżka podstawowa):
---------------------------------------

  1. Klient wybiera opcje "wpłata gotówki" 
  2. Bankomat wyświetla proponowane opcje wyboru kwoty wypłaty. 
  3. Klient wybiera jedną z opcje szybkiego wyboru kwoty i ją zatwierdza.
  4. Bankomat sprawdza ilość dostępnych środków na karcie.
  5. Bankomat potwierdza prawidłowość wybranej kwoty. 
  6. Bankomat wyświetla pytanie " Czy wydrukować potwierdzenie dokonania wypłaty".
  7. Klient wybiera opcje wydruku potwierdzenia .
  8. Bankomat wydaje kartę.
  9. Bankonat wyświetla komunikat o konieczniośći zabrania karty.
  10 Kilient zabiera kartę.
  11.Bankomat wydaje gotówkę.
  12.Bankonat wyświetla komunikat o konieczniośći zabrania gotówki.
  13.Kilient zabiera gotówkę.
  14.Bankomat drukuje potwierdzenie.
  15.Klient zabiera potwierdzenie.  
  

Rozszerzenia (ścieżki alternatywne):
------------------------------------	

 

 *a. W dowolnym czasie, dotyczy sytuacji kiedy system zawiesza się:
	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.	
	2. Transakcja zostaje przerwana.
1-9a.Kilient anuluje trasakcje:
		1.Klient anujuje transakcie za pomocą czerwonego przycisku.
		2.Transakcja zostaje przerwana.
		3.Bankomat oddaje karte.

 1a Klient wybiera inną opcję niż "wypłata gotówki":
	1.Przejdz do odpowiedniego use case.

 2a. Bankomat nie wyświetla opcji wyboru kwoty:
	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.
	

 3a. Klient wybiera opcje wprowadzenia własnej kwoty:
	1. Bankomat wyświetla ekran do wpisania kwoty.
	2. Klient wprowadza własną kwotę i zatwierdza ją.

 4a.Bankomat nie może sprawdidz ilości dostępnych śriodków na karcie:
	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.

 5a. Bankomat wyświetla komuniokat o podaniu błędnej kwoty:
 	5a.a Bankomat wyświetla komunikat o niepowodzeniu transakcji. 

 6a.Bankomat nie wyświetla pytanie " Czy wydrukować potwierdzenie dokonania wypłaty":
	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.

 7a. Kilent wybiera opcje bez wydruku potwierdzenia:
 	1.Przejdz do kroku 8 scenariusza głownego.

 8a.Bankomat nie wydaje karty:
 	1.Bankomat wyświetla informacje o awarii i nr kontaktowy do banku.

 9a Bankomat niewyświetla komunikatu o konieczniośći zabrania karty:
	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.

 10a Klient nie zabiera karty:
 	1.po upływię 40 sekund bankomat wciąga kartę.
 	2.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.

 11a.Bankomat nie wydaje gotówki:
	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.


 12a.Bankomat nie wyświetla komunikatu o konieczniośći zabrania gotówki:
 	1.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.

 13a.Kilent nie zabiera gotówki:
	1.Bankomat po upływie 30 sec wciąga pieniądze.
	2.Zakończenie transakcij.
  
14a.Bankomat nie drukuje potwierdzenia:
	1.Bankomat wyświetla komunikat o braku możliwości wydrukowania potwierdzenia.
	2.Bankomat wyświetla komunikat o awarii i wyświetla  nr kontaktowy do banku.
	

Wymagania specjalne:
--------------------
  

  - Możliwość interfejsu wielojęzycznego.

  -Ekran dotykowy służący do wyświetlania komunikatów i umożliwiający wprowadzanie danych.



Wymagania technologiczne oraz ograniczenia na wprowadzane dane:
---------------------------------------------------------------
*a System ma stałe połączenie z bazą danych banku.

*b Klawiatura numeryczna T9 z dodatkowymi przyciskami żółtym, zielonym i czerwonym.

*a*b.Transakcja anulowana za pomocą czerwonego przycisku z klawiatury nymerucznej.

1a. wybór opcji za pomocą ekranu dotykowego.

3a. Kwota wypłaty gotówki wybrana z ekranu dotykowego lub z klawiatury.

5a. Ograniczenia dotyczące kwoty wypłaty:
	-Możliwość wypłaty kwoty jedynie większej lub równej 10 złotych.
	-Mozliwość wypłaty kwoty nie wiekszej niż 10 000 złotych.


 

 
 

Kwestie otwarte:
----------------

  - Wypłata waluty innej niż PLN.

 
